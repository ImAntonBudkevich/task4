class Container {
  constructor(parent, ...classes) {
    this.classes = classes;
    this.parent = document.querySelector(`${parent}`);
    this.newEl = document.createElement(`div`);
  }

  elClasses() {
    if (this.classes.length !== 0) {
      this.classes.forEach((cls) => {
        this.newEl.classList.add(cls);
      });
    }
  }

  elRender() {
    this.parent.append(this.newEl);
  }
}

class DivImg extends Container {
  constructor(parent, src, imgClass, ...classes) {
    super(parent, ...classes);

    this.src = src;
    this.imgClass = imgClass;
  }

  imgRender() {
    this.newEl.innerHTML = `
    <img class="${this.imgClass}" src="${this.src}">
    `;
    this.parent.append(this.newEl);
  }
}

class DivHTML extends Container {
  constructor(parent, HTMLText, ...classes) {
    super(parent, ...classes);

    this.text = HTMLText;
  }

  textRender() {
    this.newEl.innerHTML = `
    ${this.text}
    `;
    this.parent.append(this.newEl);
  }
}

const avatar = new DivImg('.leftwr', 'img/IMG_3713.JPG', 'ava', 'photo');
avatar.elClasses();
avatar.imgRender();

const CV = new DivHTML(
  '.leftwr',
  `                <h2>Контактная информация</h2>
  <p>
      <p>123456, Moscow,</p>
      <p>Lenin St. 1, apt.2</p>
      <p>E-mail: aleksey@nail.ru</p>
      <p>Tel. home: (495) 555-55-55</p>
      <p>Tel. mobile: 8-ХХХ-555-55-55</p>
  </p>`,
  'info'
);
CV.elClasses();
CV.textRender();

const naming = new DivHTML(
  '.rigwr',
  `                <h1>Рокотов Валерий</h1>
<h4>Желаемая должность: Инженер</h4>
<h4>Желаемый уровень дохода: 60 тыс. рублей</h4>
`,
  'name'
);
naming.elClasses();
naming.textRender();

const achiv = new Container('.rigwr', 'achiv');
achiv.elClasses();
achiv.elRender();

const about = new DivHTML(
  '.achiv',
  `                    <h3>О себе:</h3>
<pre>
Дата рождения: 25.10.1982
Проживание: г. Санкт-Петербург, Калининский район
Готов к переезду. Готов к командировкам.
</pre>`,
  'about'
);
about.elClasses();
about.textRender();

const skils = new DivHTML(
  '.achiv',
  `<h3>Ключевые знания и навыки:</h3>
  <ul id="skilllist">
  <li>Знание принципов работы теплоэнергетического оборудования;</li>
  <li>Уверенное знание законов и нормативных актов по инженерному проектированию;</li>
  <li>Успешный опыт проектирования систем отопления на крупных объектах;</li>
  <li>Отличное знание AutoCAD и ArchiCAD;</li>
  <li>Ответственность, умение работать в команде;</li>
  <li>Стремление к развитию в профессиональной сфере.</li>
  </ul>`,
  'skill'
);
skils.elClasses();
skils.textRender();

const experience = new DivHTML(
  '.rigwr',
  `                <h3>Опыт работы:</h3>
  <div>
  <h4>11.2009–06.2015 Инженер по эксплуатации зданий</h4>
  
  <h6>ООО «Вектор» (www.vektor.com), г. Санкт-Петербург</h6>
  
  <p>Сфера деятельности компании: техническое обслуживание объектов коммерческой недвижимости</p>
  <ul>
  <li>Организация эксплуатации систем отопления в зданиях;</li>
  <li>Поддержка бесперебойной работы оборудования;</li>
  <li>Ведение технической документации.</li>
  </ul>
  </div>
  <div>
  <h4>11.2015–06.2021 Инженер по эксплуатации зданий</h4>
  
  <h6>ООО «Вектор» (www.vektor.com), г. Санкт-Петербург</h6>
  
  <p>Сфера деятельности компании: техническое обслуживание объектов коммерческой недвижимости</p>
  <ul>
  <li>Организация эксплуатации систем отопления в зданиях;</li>
  <li>Поддержка бесперебойной работы оборудования;</li>
  <li>Ведение технической документации.</li>
  </ul>`,
  'exp'
);
experience.elClasses();
experience.textRender();

const buttons = new Container('.rigwr', 'btns');
buttons.elClasses();
buttons.elRender();

const skillch = new DivHTML(
  '.btns',
  `                    <div class="skilbtns">
<button id="chsk">Change skill</button>
<button id="adsk">Add skill</button>
</div>
<p id="newsk">Type Here</p>`,
  'skilch'
);
skillch.elClasses();
skillch.textRender();

const workch = new DivHTML(
  '.btns',
  `                    <div class="workbtns">
  <button id="chwr">Change work</button>
  <button id="adwr">Add work</button>
  </div>
  <p id="newwr">Type Here</p>`,
  'workch'
);
workch.elClasses();
workch.textRender();

const backCh = new DivHTML('.btns', `<button id="slid">backgr</button>`);
backCh.elClasses();
backCh.textRender();

/////////////////////////////////////////////////////////////////////////////////////

const wrapper = document.querySelector('.wrapper');
const toggle = document.getElementById('slid');
const skillCh = document.querySelector('#chsk');
const workCh = document.querySelector('#chwr');
const fieldSk = document.querySelector('#newsk');
const fieldWr = document.querySelector('#newwr');
const ul = document.querySelector('#skilllist');
const wrDiv = document.querySelector('.exp');
const addSk = document.querySelector('#adsk');
const addWr = document.querySelector('#adwr');

toggle.addEventListener('click', () => {
  if (wrapper.style.background === 'white' || wrapper.style.background === '') {
    wrapper.style.background = 'aliceblue';
  } else wrapper.style.background = 'white';
});

skillCh.addEventListener('click', function () {
  if (skillCh.textContent === 'Change skill') {
    skillCh.textContent = 'save';
    fieldSk.contentEditable = true;
    fieldSk.focus();
  } else {
    skillCh.textContent = 'Change skill';
    fieldSk.contentEditable = false;
  }
});

fieldSk.addEventListener('blur', () => {
  skillCh.textContent = 'Change skill';
  fieldSk.contentEditable = false;
});

addSk.addEventListener('click', () => {
  let newLi = document.createElement('li');
  newLi.innerHTML = fieldSk.innerHTML;
  ul.append(newLi);
  fieldSk.innerHTML = 'Type Here';
});

workCh.addEventListener('click', function () {
  if (this.textContent === 'Change work') {
    this.textContent = 'save';
    fieldWr.contentEditable = true;
    fieldWr.focus();
  } else {
    this.textContent = 'Change work';
    fieldWr.contentEditable = false;
  }
});

fieldWr.addEventListener('blur', () => {
  workCh.textContent = 'loading';
  fieldWr.contentEditable = false;
});

addWr.addEventListener('click', () => {
  let newWr = document.createElement('div');
  newWr.innerText = fieldWr.innerHTML;
  wrDiv.append(newWr);
  fieldWr.innerHTML = 'Type Here';
});

const Conteiner = function (parent) {
  this.parent = parent;

  this.naming = function () {
    console.log(`my parent is ${parent}`);
  };
};

const Image = function (parent, src) {
  Conteiner.apply(this, arguments);

  this.src = src;
  this.pict = function () {
    console.log(`look at me ${this.src}`);
  };
};

Image.prototype = Object.create(Conteiner.prototype);
Image.prototype.constructor = Image;
Conteiner.prototype.voice = function () {
  console.log(`he too`);
};

console.log(Image);

const Box = new Conteiner('Oleg');
Box.naming();
Box.voice();

const Nature = new Image(
  'gena',
  'https://codesandbox.io/s/flamboyant-ishizaka-7un4t?file=/src/index.js'
);
Nature.naming();
Nature.voice();
Nature.pict();

console.log(Nature);
